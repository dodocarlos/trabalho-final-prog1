package me.cl.sistema.item;

import java.util.stream.Collectors;
import me.cl.sistema.pessoa.Pessoa;
import me.cl.sistema.utils.Listas;

public class Produto extends Item {

    int qtdEmEstoque;
    Pessoa fornecedor;

    public Produto() {

    }

    public Produto(int cod) {
        this.cod = cod;
    }

    public int getQtdEmEstoque() {
        return this.qtdEmEstoque;
    }

    public boolean setQtdEmEstoque(int novaQuantidade) {
        //Vamos poder ter estoque 0, j� que o estoque do item pode ter acabado
        if (novaQuantidade >= 0) {
            this.qtdEmEstoque = novaQuantidade;
            return true;
        } else {
            return false;
        }
    }

    public Pessoa getFornecedor() {
        return this.fornecedor;
    }

    public boolean setFornecedor(Pessoa novoFornecedor) {
        if (novoFornecedor != null) {
            this.fornecedor = novoFornecedor;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return super.toString() + ", Quantidade em estoque: " + this.getQtdEmEstoque();
    }

    @Override
    public String toFileString() {
        return super.toFileString() + ";" + this.getQtdEmEstoque() + ";" + this.getFornecedor().getCod();
    }

    private Pessoa retornaFornecedor(int codFornecedor){
        try{
            return Listas.getInstance().pessoas().parallelStream().filter(pessoa -> pessoa.getCod() == codFornecedor)
                            .collect(Collectors.toList())
                            .get(0);
        }catch(Exception e){
            return null;
        }
    }
    
    @Override
    public boolean loadDataFromString(String dataString) {
        try {
            String[] data = dataString.split(";");
            this.cod = Integer.parseInt(data[0]);
            this.setDescricao(data[2]);
            this.setValor(Float.parseFloat(data[3]));
            this.setQtdEmEstoque(Integer.parseInt(data[4]));
            this.setFornecedor(retornaFornecedor(Integer.parseInt(data[5])));

            return true;
        } catch (Exception ex) {
            return false;
        }
    }

}
