package me.cl.sistema.item;

public class Servico extends Item {
    String prestador;

    public Servico(int codigo) {
        this.cod = codigo;
    }

    public Servico(){
        
    }
    
    public String getPrestador() {
        return this.prestador;
    }

    public boolean setPrestador(String novoPrestador) {
        if (!novoPrestador.trim().isEmpty()) {
            this.prestador = novoPrestador;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return super.toString() + ", Prestador: " + this.getPrestador();
    }

    @Override
    public boolean loadDataFromString(String dataString) {
        try {
            String[] data = dataString.split(";");

            this.cod = Integer.parseInt(data[0]);
            this.setDescricao(data[2]);
            this.setValor(Float.parseFloat(data[3]));
            this.setPrestador(data[4]);

            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    @Override
    public String toFileString() {
        return super.toFileString() + ";" + this.getPrestador();
    }

}
