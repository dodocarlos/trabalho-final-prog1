package me.cl.sistema.item;

import java.util.stream.Collectors;
import me.cl.sistema.utils.Listas;

public class Item {
	int cod;
	String descricao;
	float valor;
	
	public Item() {

	}
	
	public float getValor() {
		return this.valor;
	}
	
	public boolean setValor(float novoValor) {
		if (novoValor > 0) {
			this.valor = novoValor;
			return true;
		} else {
			return false;
		}
	}
	
	public String getDescricao() {
		return this.descricao;
	}
	
	public boolean setDescricao(String novaDescricao) {
		if (!novaDescricao.trim().isEmpty()) {
			this.descricao = novaDescricao;
			return true;
		} else {
			return false;
		}
	}
	
	public int getCod() {
		return this.cod;
	}
	
	@Override
	public String toString() {
		return "Descriçãoo: " + this.getDescricao() + ", Valor: " + this.getValor();
	}
	
	public String toFileString() {
		return this.getCod() + ";" + (this instanceof Produto ? "P" : "S") + ";" 
                        + this.getDescricao() + ";" + this.getValor(); 
	}	
	
	/** METODOS EST�TICOS **/
	
	/**
	 * M�todo para carregar um objeto por uma string
	 * @param dataString String do objeto(separada por ";")
	 * @return Retornar o objeto criado tendo a dataString como base
	 */
	public boolean loadDataFromString(String dataString) {
		try {
			String[] data = dataString.split(";");
			
			this.cod = Integer.parseInt(data[0]);
			this.setDescricao(data[2]);
			this.setValor(Float.parseFloat(data[3]));
			
			return true;
		} catch (Exception ex) {
			return false;
		}
	}
        
        public static Item retornaItemPeloCodigo(int codItem){
            try{
                return Listas.getInstance().itens().parallelStream().filter(i -> i.getCod() == codItem)
                             .collect(Collectors.toList())
                             .get(0);
            } catch(Exception ex){
                return null;
            }
        }
}
