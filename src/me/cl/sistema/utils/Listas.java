package me.cl.sistema.utils;

import java.util.ArrayList;

import me.cl.sistema.item.Item;
import me.cl.sistema.notafiscal.NotaFiscal;
import me.cl.sistema.pedido.ItemPedido;
import me.cl.sistema.pedido.Pedido;
import me.cl.sistema.pessoa.Pessoa;

public class Listas {
	
	private static Listas instance;
	
	private ArrayList<Pessoa> pessoas;
	private ArrayList<Item> itens;
	private ArrayList<ItemPedido> itensPedidos;
	private ArrayList<Pedido> pedidos;
	private ArrayList<NotaFiscal> notasFiscais;

	
	private Listas() {
		this.pessoas = new ArrayList<>();
		this.itens = new ArrayList<>();
		this.itensPedidos = new ArrayList<>();
		this.pedidos = new ArrayList<>();
		this.notasFiscais = new ArrayList<>();
	}
	
	//Vou fazer um get para as listas para caso precise alterar algo nao ter que sair mudando em todos os lugares
	public ArrayList<Pessoa> pessoas(){
		return this.pessoas;
	}
	
	public ArrayList<Item> itens(){
		return this.itens;
	}
	
	public ArrayList<ItemPedido> itensPedidos(){
		return this.itensPedidos;
	}
	
	public ArrayList<Pedido> pedidos(){
		return this.pedidos;
	}
	
	public ArrayList<NotaFiscal> notasFiscais(){
		return this.notasFiscais;
	}
	
	public static Listas getInstance() {
		if (instance == null) instance = new Listas();
		
		return instance;
	}
	
}
