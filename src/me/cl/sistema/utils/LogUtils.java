package me.cl.sistema.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import javax.swing.JOptionPane;

public class LogUtils {
	
	public static void writeLogLine(String prefixo, String line) {
		File logFile = new File(Consts.CONST_DIR_BASE + "/log" + Consts.CONST_FILE_EXTENSION);
		
		try {
			if (!logFile.exists()) logFile.createNewFile();
			BufferedWriter writer = new BufferedWriter(new FileWriter(logFile, true));
			writer.write("[" + new Date() + "] " + prefixo  + " " + line);
			writer.newLine();
			
			writer.close();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Erro ao criar o arquivo de log!");
		}
	}
	
}
