package me.cl.sistema.utils;

public enum ObjetoCarregar {
	
	PESSOAS("Pessoas"), 
	ITENS("Itens"), 
	PEDIDOS("Pedidos"), 
	ITENSPEDIDOS("ItensPedidos"),
	NOTASFISCAIS("NotasFiscais");

	private String fileName;
	
	private ObjetoCarregar(String fileName) {
		this.fileName = fileName;
	}
	
	public String getFileName() {
		return this.fileName;
	}
	
}
