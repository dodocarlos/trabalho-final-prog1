package me.cl.sistema;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Collectors;

import me.cl.sistema.item.Item;
import me.cl.sistema.item.Produto;
import me.cl.sistema.item.Servico;
import me.cl.sistema.notafiscal.NotaFiscal;
import me.cl.sistema.pedido.ItemPedido;
import me.cl.sistema.pedido.Pedido;
import me.cl.sistema.pessoa.Pessoa;
import me.cl.sistema.pessoa.PessoaFisica;
import me.cl.sistema.pessoa.PessoaJuridica;
import me.cl.sistema.utils.Consts;
import me.cl.sistema.utils.Listas;
import me.cl.sistema.utils.LogUtils;
import me.cl.sistema.utils.ObjetoCarregar;

public class Sistema {

    private ArrayList<Pessoa> pessoas = Listas.getInstance().pessoas();
    private ArrayList<Item> itens = Listas.getInstance().itens();
    private ArrayList<ItemPedido> itensPedidos = Listas.getInstance().itensPedidos();
    private ArrayList<Pedido> pedidos = Listas.getInstance().pedidos();
    private ArrayList<NotaFiscal> notasFiscais = Listas.getInstance().notasFiscais();

    private void carregaObjeto(ObjetoCarregar tipoObjeto) {
        String fileName = Consts.CONST_DIR_BASE + "/" + tipoObjeto.getFileName() + Consts.CONST_FILE_EXTENSION;
        File filePessoas = new File(fileName);

        //Se n�o existe o arquivo, criamos ele antes
        if (!filePessoas.exists()) {
            try {
                filePessoas.createNewFile();
            } catch (IOException e) {
                LogUtils.writeLogLine("{ARQUIVO}", "Não foi possível criar o arquivo (" + fileName + "): " + e.getMessage());
            }
        } else {
            //S� vamos carregar os dados quando o arquivo n�o foi criado nessa execu��o, senao nao tem necessidade
            //j� que nao vamos ter registros
            try {
                FileReader fileReader = new FileReader(filePessoas);
                try (BufferedReader buffReader = new BufferedReader(fileReader)) {
                    String line;
                    while ((line = buffReader.readLine()) != null) {
                        if (!line.trim().isEmpty()) {
                            switch (tipoObjeto) {
                                case PESSOAS:
                                    String tipoPessoa = line.split(";")[1];
                                    
                                    if (tipoPessoa.equalsIgnoreCase("F")){
                                        PessoaFisica pessoaF = new PessoaFisica();
                                        if (pessoaF.loadDataFromString(line)) {
                                            this.pessoas.add(pessoaF);
                                        } else {
                                            pessoaF = null;
                                        }
                                    } else {
                                        PessoaJuridica pessoaJ = new PessoaJuridica();
                                        if (pessoaJ.loadDataFromString(line)) {
                                            this.pessoas.add(pessoaJ);
                                        } else {
                                            pessoaJ = null;
                                        }                                        
                                    }
                                    break;

                                case ITENS:
                                    String tipoItem = line.split(";")[1];
                                    
                                    if (tipoItem.equalsIgnoreCase("P")){
                                        Produto produto = new Produto();

                                        if (produto.loadDataFromString(line)) {
                                            this.itens.add(produto);
                                        } else {
                                            produto = null;
                                        }
                                    } else {
                                        Servico servico = new Servico();

                                        if (servico.loadDataFromString(line)) {
                                            this.itens.add(servico);
                                        } else {
                                            servico = null;
                                        }
                                    }
                                    
                                    break;

                                case ITENSPEDIDOS:
                                    ItemPedido itemPedido = new ItemPedido();

                                    if (itemPedido.loadDataFromString(line)) {
                                        this.itensPedidos.add(itemPedido);
                                    } else {
                                        itemPedido = null;
                                    }
                                    break;

                                case PEDIDOS:
                                    Pedido pedido = new Pedido();

                                    if (pedido.loadDataFromString(line)) {
                                        this.pedidos.add(pedido);
                                    } else {
                                        pedido = null;
                                    }
                                    break;

                                case NOTASFISCAIS:
                                    NotaFiscal notaFiscal = new NotaFiscal();

                                    if (notaFiscal.loadDataFromString(line)) {
                                        this.notasFiscais.add(notaFiscal);
                                    } else {
                                        notaFiscal = null;
                                    }
                                    break;
                            }

                        }
                    }
                }
            } catch (IOException e) {
                LogUtils.writeLogLine("{ARQUIVO}", "Erro ao ler o arquivo " + "(" + fileName + "): " + e.getMessage());
            }
        }
    }

    private void salvarDados(ObjetoCarregar tipoObjeto) {
        String fileName = Consts.CONST_DIR_BASE + "/" + tipoObjeto.getFileName() + Consts.CONST_FILE_EXTENSION;
        File file = new File(fileName);

        try {
            if (!file.exists()) {
                file.createNewFile();
            }
        } catch (IOException e) {
            LogUtils.writeLogLine("{ARQUIVO}", "Erro ao criar o arquivo " + "(" + fileName + "): " + e.getMessage());
        }

        try {
            FileWriter fw = new FileWriter(file);
            try (BufferedWriter bw = new BufferedWriter(fw)) {
                switch (tipoObjeto) {
                    case PESSOAS:
                        for (Pessoa p : this.pessoas.stream().filter(p -> p instanceof PessoaFisica).collect(Collectors.toList())) {
                            bw.write(p.toFileString());
                            bw.newLine();
                        }
                        for (Pessoa p : this.pessoas.stream().filter(p -> p instanceof PessoaJuridica).collect(Collectors.toList())) {
                            bw.write(p.toFileString());
                            bw.newLine();
                        }
                        break;
                        
                    case ITENS:
                        for (Item item : this.itens.stream().filter(i -> i instanceof Produto).collect(Collectors.toList())) {
                            bw.write(item.toFileString());
                            bw.newLine();
                        }

                        for (Item item : this.itens.stream().filter(i -> i instanceof Servico).collect(Collectors.toList())) {
                            bw.write(item.toFileString());
                            bw.newLine();
                        }
                        break;

                    case ITENSPEDIDOS:
                        for (ItemPedido ip : this.itensPedidos) {
                            bw.write(ip.toFileString());
                            bw.newLine();
                        }
                        break;

                    case PEDIDOS:
                        for (Pedido ped : this.pedidos) {
                            bw.write(ped.toFileString());
                            bw.newLine();
                        }
                        break;

                    case NOTASFISCAIS:
                        for (NotaFiscal nf : this.notasFiscais) {
                            bw.write(nf.toFileString());
                            bw.newLine();
                        }
                        break;

                }
            }
        } catch (Exception e) {
            LogUtils.writeLogLine("{ARQUIVO}", "Erro ao salvar o arquivo " + "(" + fileName + "): " + e.getMessage());
        }

    }

    public void carregaDados() {
        carregaObjeto(ObjetoCarregar.PESSOAS);
        carregaObjeto(ObjetoCarregar.ITENS);
        carregaObjeto(ObjetoCarregar.ITENSPEDIDOS);
        carregaObjeto(ObjetoCarregar.PEDIDOS);
        carregaObjeto(ObjetoCarregar.NOTASFISCAIS);
    }

    public boolean efetivaAlteracoes() {
        //Nesse metodo lindo vamos ter que varrer as listas e ir salvando no arquivo.
        //Pensei em trabalhar dessa forma, pois nao vamos precisar ficar escrevendo no arquivo sempre.
        //Podemos criar os metodos para cada tipo fora daqui e ir chamando eles aqui.

        //Outro ponto que eu pensei que poderiamos fazer � tratar como se estivessemos dentro de uma trnasa�ao,
        //com isso vamos garantir que salve todos os dados e n�o fique lixo nos arquivos.
        //Para isso podemos usar um try que circule todo o metodo. Durante as grava��es criamos arquivos temporarios para garantir
        //que tudo ocorra bem. Se der tudo certo, apagamos os aquivos antigos(podemos pensar num backup) e renomeamos os novos.
        //Caso der uma excessao, simplesmente paramos a grava��o, alertamos o usuario, geramos log e apagamos os arquivos temp.
        
        salvarDados(ObjetoCarregar.PESSOAS);
        salvarDados(ObjetoCarregar.ITENS);
        salvarDados(ObjetoCarregar.ITENSPEDIDOS);
        salvarDados(ObjetoCarregar.PEDIDOS);
        salvarDados(ObjetoCarregar.NOTASFISCAIS);        
        return true;
    }

}
