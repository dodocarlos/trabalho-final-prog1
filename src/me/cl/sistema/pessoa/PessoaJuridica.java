package me.cl.sistema.pessoa;

public class PessoaJuridica extends Pessoa {
	String CNPJ;
	String razaoSocial;

	public PessoaJuridica(int cod, String nome, String CNPJ, String razaoSocial, String telefone, String email,
			boolean fornecedor) {
		super(nome, telefone, email, fornecedor);
                this.cod = cod;
		this.CNPJ = CNPJ;
		this.razaoSocial = razaoSocial;
	}

	public PessoaJuridica() {
		
	}

	public String getCNPJ() {
		return this.CNPJ;
	}

	public boolean setRazaoSocial(String novaRazaoSocial) {
		if (!novaRazaoSocial.trim().isEmpty()) {
			this.razaoSocial = novaRazaoSocial;
			return true;
		} else {
			return false;
		}
	}

	public String getRazaoSocial() {
		return this.razaoSocial;
	}

	@Override
	public String toString() {
		return super.toString() + ", CNPJ: " + this.getCNPJ() + ", Raz�o Social: " + this.getRazaoSocial();
	}

	@Override
	public String toFileString() {
		return super.toFileString() + ";" + this.getCNPJ() + ";" + this.getRazaoSocial();
	}

	@Override
	public boolean loadDataFromString(String linhaDados) {
		try {
			String[] dados = linhaDados.split(";");
			if (super.loadDataFromString(linhaDados)) {
				this.CNPJ = dados[6];
				this.razaoSocial = dados[7];

				return true;
			} else
				return false;
		} catch (Exception ex) {
			return false;
		}
	}

}
