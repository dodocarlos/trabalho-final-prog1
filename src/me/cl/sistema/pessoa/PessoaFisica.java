package me.cl.sistema.pessoa;

public class PessoaFisica extends Pessoa{
	String cpf;
	String rg;

	public PessoaFisica(int cod, String nome, String cpf, String rg, String telefone, String email, boolean fornecedor) {
		super(nome, telefone, email, fornecedor);
                this.cod = cod;
		this.cpf = cpf;
		this.rg = rg;
	}
	
	public PessoaFisica() {
		
	}	
	
	public String getCPF() {
		return this.cpf;
	}
	
	public String getRG() {
		return this.rg;
	}
	
	@Override
	public String toString() {
		return super.toString() + ", CPF: " + this.getCPF() + ", RG: " + this.getRG();
	}
	
	@Override
	public String toFileString() {
		return super.toFileString() + ";" +
			   this.getCPF() + ";" +
			   this.getRG();
	}
	
	@Override
	public boolean loadDataFromString(String linhaDados) {
		try {
			String[] dados = linhaDados.split(";");
			if (super.loadDataFromString(linhaDados)) {
				this.cpf = dados[6];
				this.rg = dados[7];
				
				return true;
			} else return false;
		} catch(Exception ex) {
			return false;
		}
	}
		
}
