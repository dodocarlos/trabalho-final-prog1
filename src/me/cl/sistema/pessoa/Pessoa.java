package me.cl.sistema.pessoa;

import java.util.stream.Collectors;
import me.cl.sistema.utils.Listas;

public class Pessoa {

    String nome;
    String email;
    String telefone;
    boolean fornecedor;
    int cod;

    public Pessoa(String nome, String telefone, String email, boolean fornecedor) {
        this.nome = nome;
        this.email = email;
        this.telefone = telefone;
        this.fornecedor = fornecedor;
    }

    public Pessoa() {

    }

    public Pessoa(int cod) {
        this.cod = cod;
    }

    public int getCod() {
        return this.cod;
    }

    public String getNome() {
        return this.nome;
    }

    public boolean setNome(String novoNome) {
        if (!novoNome.trim().isEmpty()) {
            this.nome = novoNome;
            return true;
        } else {
            return false;
        }
    }

    public String getEmail() {
        return this.email;
    }

    public boolean setEmail(String novoEmail) {
        //Verificiar se tem necessidade de fazer alguma verificacao, pois a principio a pessoa pode ter 
        //apagado o email, portanto ele pode ser vazio.
        //Talvez seria interessante consistir se ja nao existe um cliente com esse email.
        this.email = novoEmail;
        return true;
    }

    public String getTelefone() {
        return this.telefone;
    }

    public boolean setTelefone(String novoTelefone) {
        if (!novoTelefone.trim().isEmpty()) {
            this.telefone = novoTelefone;
            return true;
        } else {
            return false;
        }
    }

    public boolean getFornecededor() {
        return this.fornecedor;
    }

    public boolean setEhFornecedor(boolean ehFornecedor) {
        this.fornecedor = ehFornecedor;
        return true;
    }

    public String toString() {
        return "Nome: " + this.getNome()
                + ", Email: " + this.getEmail()
                + ", Telefone: " + this.getTelefone()
                + ", � fornecedor: " + this.getFornecededor();
    }

    public String toFileString() {
        return this.getCod() + ";"
                + (this instanceof PessoaFisica ? "F" : "J") + ";"
                + this.getNome() + ";"
                + this.getEmail() + ";"
                + this.getTelefone() + ";"
                + String.valueOf(this.getFornecededor());
    }

    public boolean loadDataFromString(String linhaDados) {
        try {
            String[] dados = linhaDados.split(";");

            //Carrega a pessoa com os dados passados por parametro
            this.cod = Integer.parseInt(dados[0]);
            this.setNome(dados[2]);
            this.setEmail(dados[3]);
            this.setTelefone(dados[4]);
            this.setEhFornecedor(Boolean.parseBoolean(dados[5]));

            return true;
        } catch (ExceptionInInitializerError ex) {
            //Se der alguem erro, como por exemplo a String for inv�lida
            return false;
        }
    }
    
    public static Pessoa retornaPessoaPeloCodigo(int codPessoa){
        try{
            return Listas.getInstance().pessoas().parallelStream()
                    .filter(p -> p.getCod() == codPessoa)
                    .collect(Collectors.toList())
                    .get(0);
        } catch(Exception e){
            return null;
        }
    }

}
