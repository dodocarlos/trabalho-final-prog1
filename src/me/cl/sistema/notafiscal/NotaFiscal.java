package me.cl.sistema.notafiscal;

import java.util.stream.Collectors;

import me.cl.sistema.pessoa.Pessoa;
import me.cl.sistema.utils.Listas;

public class NotaFiscal {
	int codPedido;
	float valorTotal;
	Pessoa cliente;
	
	public float getValorTotal() {
		return valorTotal;
	}
	
	public boolean setValorTotal(float valorTotal) {
		if (valorTotal > 0) {
			this.valorTotal = valorTotal;
			return true;
		} else {
			return false;
		}
	}
	
	public Pessoa getCliente() {
		return cliente;
	}
	
	public boolean setCliente(Pessoa cliente) {
		if (cliente != null) {
			this.cliente = cliente;
			return true;
		} else {
			return false;
		}
	}
	
	public boolean setPedido(int novoPedido) {
		if (novoPedido > 0) {
			this.codPedido = novoPedido;
			return true;
		} else {
			return false;
		}
	}
	
	public int getPedido() {
		return this.codPedido;
	}
	
	public String toFileString() {
		return this.getPedido() + ";" + this.getCliente().getCod() + ";" + this.getValorTotal(); /*ListaPedidos.getPedido(this.codPedido).getValorTotal*/
	}
	
	public void definePessoa(int codPessoa) {
		try {
			this.cliente = Listas.getInstance().pessoas().parallelStream()
					.filter(pessoa -> pessoa.getCod() == codPessoa)
					.collect(Collectors.toList()).get(0);
		} catch (Exception ex) {
			this.cliente = null;
		}
	}
	
	public boolean loadDataFromString(String dataString) {
		try {
			String[] data = dataString.split(";");
			
			this.setPedido(Integer.parseInt(data[0]));
			definePessoa(Integer.parseInt(data[1]));
			this.setValorTotal(Float.parseFloat(data[2]));
			
			return true;			
		} catch (Exception ex) {
			return false;
		}
	}
	
}
