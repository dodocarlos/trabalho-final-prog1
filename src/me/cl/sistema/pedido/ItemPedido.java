package me.cl.sistema.pedido;

import java.util.ArrayList;
import java.util.stream.Collectors;

import me.cl.sistema.item.Item;
import me.cl.sistema.utils.Listas;

public class ItemPedido {

    private int codPedido;
    private int codItem;
    private int qtdItem;
    private Item item;

    public ItemPedido(int qtdItem, Item item) {
        this.qtdItem = qtdItem;
        this.item = item;
    }

    public ItemPedido() {

    }

    public int getCodPedido() {
        return this.codPedido;
    }

    public int getQtdItem() {
        return this.qtdItem;
    }

    public void setQtdItem(int qtdItem) {
        this.qtdItem = qtdItem;
    }

    public float getValorItem() {
        return this.item.getValor() * this.qtdItem;
    }

    public int getCodItem() {
        return this.codItem;
    }

    public void setCodPedido(int codPedido) {
        this.codPedido = codPedido;
    }

    public String toFileString() {
        return this.getCodPedido() + ";" + this.getCodItem() + ";" + this.item.getCod() + ";" + this.getQtdItem();
    }

    public boolean loadDataFromString(String dataString) {
        try {
            String[] data = dataString.split(";");

            this.codPedido = Integer.parseInt(data[0]);
            this.codItem = Integer.parseInt(data[1]);
            this.qtdItem = Integer.parseInt(data[2]);

            defineItemDoPedido(this.codItem);

            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    private void defineItemDoPedido(int codItem) {
        ArrayList<Item> itens = Listas.getInstance().itens();

        try {
            this.item = itens.parallelStream().filter(item -> item.getCod() == codItem).collect(Collectors.toList()).get(0);
        } catch (Exception e) {
            this.item = null;
        }
    }

    public Item getItem() {
        return item;
    }

}
