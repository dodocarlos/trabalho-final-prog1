package me.cl.sistema.pedido;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.stream.Collectors;
import me.cl.sistema.pessoa.Pessoa;

import me.cl.sistema.utils.Listas;

public class Pedido {

    private int cod;
    private Date dataEmissao;
    private Date dataExpedicao;
    private ArrayList<ItemPedido> itensPedido;
    private Pessoa cliente;

    public Pedido(int cod, Date dataEmissao, Date dataExpedicao) {
        this.cod = cod;
        this.dataEmissao = dataEmissao;
        this.dataExpedicao = dataExpedicao;
        itensPedido = new ArrayList<>();
    }

    public Pedido() {
        itensPedido = new ArrayList<>();
    }

    public int getCod() {
        return cod;
    }

    public Date getDataEmissao() {
        return dataEmissao;
    }

    public void setDataEmissao(Date dataEmissao) {
        this.dataEmissao = dataEmissao;
    }

    public Date getDataExpedicao() {
        return dataExpedicao;
    }

    public void setDataExpedicao(Date dataExpedicao) {
        this.dataExpedicao = dataExpedicao;
    }

    public float getValorTotal() {
        float valorTotal = 0;

        for (ItemPedido itemPedido : itensPedido) {
            valorTotal += itemPedido.getValorItem();
        }

        return valorTotal;
    }

    private boolean carregaItensPedido() {
        try {
            ArrayList<ItemPedido> itensPedido = Listas.getInstance().itensPedidos();
            //Varre paralelamente(n�o sequencial) os itens de pedido, filtrando pelo pedido e adiciona na lista.
            //Nesse caso n�o vai ter problema varrer eles dessa maneira, ja que n�o nos importar a sequencia, mas sim os itens
            itensPedido.parallelStream().filter(itemPedido -> itemPedido.getCodPedido() == this.cod)
                    .collect(Collectors.toList())
                    .forEach(itemPedido -> {
                        this.itensPedido.add(itemPedido);
                    });
        } catch (Exception ex) {
            return false;
        }

        return true;
    }

    public boolean loadDataFromString(String dataString) {
        try {
            String[] data = dataString.split(";");
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

            this.cod = Integer.parseInt(data[0]);
            this.dataEmissao = df.parse(data[1]);
            this.dataExpedicao = df.parse(data[2]);

            carregaItensPedido();

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public String toFileString() {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        return this.getCod() + ";" + df.format(dataEmissao) + ";" + df.format(dataExpedicao);
    }

    public ArrayList<ItemPedido> getItensPedido() {
        return itensPedido;
    }
    
    public void addItemPedido(ItemPedido itemPed){
        if (itemPed != null){
            this.itensPedido.add(itemPed);
        }
    }
    
    public Pessoa getCliente(){
        return this.cliente;
    }
    
    public boolean setCliente(Pessoa cliente){
        if (cliente != null){
            this.cliente = cliente;
            return true;
        } else {
            return false;
        }
    }
}
