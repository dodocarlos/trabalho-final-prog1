package me.cl.view.item;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.stream.Collectors;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import me.cl.sistema.item.Item;
import me.cl.sistema.item.Produto;
import me.cl.sistema.item.Servico;
import me.cl.sistema.utils.Listas;

public class FormConsultaItens extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2008506392569490762L;
	private JPanel contentPane;
	private JTable tblItens;

	/**
	 * Create the frame.
	 */
        
	public FormConsultaItens() {
		setTitle("Consulta de itens");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 820, 435);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JScrollPane scrollPane = new JScrollPane();
		
		JButton btnNovo = new JButton("Novo");
		btnNovo.addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        new FormCadastroItem().setVisible(true);
                        preencheTabela();
                    }                    
                });
                
		JButton btnEditar = new JButton("Editar");
                btnEditar.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        new FormCadastroItem().editarItem(retornaItemSelecionado());
                        preencheTabela();
                    }
                });
		
		JButton btnDesativar = new JButton("Desativar");
                btnDesativar.setVisible(false);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 794, Short.MAX_VALUE)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(btnNovo)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnEditar)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnDesativar)
					.addGap(575))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnNovo)
						.addComponent(btnEditar)
						.addComponent(btnDesativar))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 358, Short.MAX_VALUE)
					.addGap(0))
		);
		
		tblItens = new JTable();
		tblItens.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"C\u00F3digo", "Descri\u00E7\u00E3o", "Valor", "Fornecedor", "Prestador", "Qtd. em Est."
			}
		) {
			private static final long serialVersionUID = 6172469604673965430L;
			Class[] columnTypes = new Class[] {
				Integer.class, String.class, Float.class, String.class, String.class, Integer.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
			
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		});
                
                tblItens.setAutoCreateRowSorter(true);
                
		//Codgio
		tblItens.getColumnModel().getColumn(0).setPreferredWidth(50);
		tblItens.getColumnModel().getColumn(0).setMaxWidth(tblItens.getColumnModel().getColumn(0).getPreferredWidth());
		tblItens.getColumnModel().getColumn(0).setMinWidth(tblItens.getColumnModel().getColumn(0).getPreferredWidth());
		
		//Descricao
		tblItens.getColumnModel().getColumn(1).setPreferredWidth(500);
//		table.getColumnModel().getColumn(1).setMaxWidth(table.getColumnModel().getColumn(1).getPreferredWidth());
		
		//Valor
		tblItens.getColumnModel().getColumn(2).setPreferredWidth(60);
		tblItens.getColumnModel().getColumn(2).setMaxWidth(tblItens.getColumnModel().getColumn(2).getPreferredWidth());
		tblItens.getColumnModel().getColumn(2).setMinWidth(tblItens.getColumnModel().getColumn(2).getPreferredWidth());
		
		//Fornecedor
		tblItens.getColumnModel().getColumn(3).setPreferredWidth(500);
		tblItens.getColumnModel().getColumn(3).setPreferredWidth(500);
		
		//Prestador
		tblItens.getColumnModel().getColumn(4).setPreferredWidth(500);
		tblItens.getColumnModel().getColumn(4).setPreferredWidth(500);

		//Qtd. em estoque
		tblItens.getColumnModel().getColumn(5).setPreferredWidth(80);
		tblItens.getColumnModel().getColumn(5).setMaxWidth(tblItens.getColumnModel().getColumn(5).getPreferredWidth());
		tblItens.getColumnModel().getColumn(5).setMinWidth(tblItens.getColumnModel().getColumn(5).getPreferredWidth());
		
		tblItens.setFillsViewportHeight(true);
		scrollPane.setViewportView(tblItens);
		contentPane.setLayout(gl_contentPane);
		
		preencheTabela();
	}
	
	public void preencheTabela() {
		DefaultTableModel tableModel = (DefaultTableModel) tblItens.getModel();
                
                tableModel.setRowCount(0);
		
		for (Item item : Listas.getInstance().itens()) {
			int codigo = item.getCod();
			String desc = item.getDescricao();
			float valor = item.getValor();
			String fornecedor = "-";
			String prestador = "-";
			int qtdEmEstoque = 0;
			
			if (item instanceof Produto) {
				Produto produto = (Produto) item;
				
				if (produto.getFornecedor() != null) fornecedor = produto.getFornecedor().getNome();
				qtdEmEstoque = produto.getQtdEmEstoque();
			} else {
				Servico servico = (Servico) item;
				
				prestador = servico.getPrestador();
			}
			
			tableModel.addRow(new Object[]{codigo, desc, valor, fornecedor, prestador, qtdEmEstoque});
		}
	}
	
	public Item retornaItemSelecionado() {
		return Listas.getInstance().itens().parallelStream()
				.filter(item -> item.getCod() == (Integer) tblItens.getValueAt(tblItens.getSelectedRow(), 0))
				.collect(Collectors.toList())
				.get(0);
	}
}
