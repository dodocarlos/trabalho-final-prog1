/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.cl.control.pessoa;

import com.sun.jmx.mbeanserver.Util;
import java.util.ArrayList;
import java.util.Collections;
import me.cl.sistema.pessoa.Pessoa;
import me.cl.sistema.pessoa.PessoaFisica;
import me.cl.sistema.pessoa.PessoaJuridica;
import me.cl.sistema.utils.Listas;

/**
 *
 * @author Lucas Martendal
 */
public abstract class ControlPessoa {
    
    public static void addPessoa(String nome, boolean pessoaFisica, String CPFCNPJ,
            String RGRazaoSocial, String telefone, String email, boolean fornecedor) {
        Pessoa pes;
        
        int codPessoa = Listas.getInstance().pessoas().size() + 1;
        
        if (pessoaFisica) {
            pes = new PessoaFisica(codPessoa, nome, CPFCNPJ, RGRazaoSocial, telefone, email, fornecedor);
        } else {
            pes = new PessoaJuridica(codPessoa, nome, CPFCNPJ, RGRazaoSocial, telefone, email, fornecedor);
        }
        
//        ArrayList listaPessoas;
//        listaPessoas = Listas.getInstance().pessoas();

        Listas.getInstance().pessoas().add(pes);
    }
    
//    public static void lerPessoas
    
    public static void editPessoa(Pessoa pes, String nome, boolean pessoaFisica, String CPFCNPJ,
        String RGRazaoSocial, String telefone, String email, boolean fornecedor) {        

        pes.setNome(nome);
        pes.setTelefone(telefone);
        pes.setEmail(email);
        pes.setEhFornecedor(fornecedor);
                
//        if (pessoaFisica) {
////            pes = new PessoaFisica(codPessoa, nome, CPFCNPJ, RGRazaoSocial, telefone, email, fornecedor);
//        } else {
////            pes = new PessoaJuridica(codPessoa, nome, CPFCNPJ, RGRazaoSocial, telefone, email, fornecedor);
//        }
        
//        ArrayList listaPessoas;
//        listaPessoas = Listas.getInstance().pessoas();

//        Listas.getInstance().pessoas().add(pes);
          //nao precisa adicionar na lista pois apenas esta sendo atualizado os dados
    }
    
    
}
